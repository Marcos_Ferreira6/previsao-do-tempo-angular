export class Previsao {
  id: string;
  name: string;
  country: string;
  region: string;
  acronym: string;
  data: Data;
}

export class Data {
  date: string;
  date_br: string;
  image: string;
  text: string;
}
