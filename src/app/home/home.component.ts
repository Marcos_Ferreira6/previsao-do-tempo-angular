import { Previsao } from './model/previsao.model';
import {  HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // HttpCliente injetado para conseguir se comunicar com a API
  // Deve ser declarado o HttpClientModule no AppModule
  constructor(private http: HttpClient) { }

  TOKEN = '8225663566c26362af7c04a1ec881bd2'; // Token gerado no Site
  URL = 'http://apiadvisor.climatempo.com.br/api/v1/forecast/region/sudeste?token='; // URL para se comunicar com o site

  // Objeto Criado para receber a previsão que vem da API
  // Ctrl + clique em cima do Objeto para ver como ele foi implementado
  previsao: Previsao;

  // Array que recebe os dias previstos
  previsoes: any = [];

  ngOnInit() {
    this.previsao = new Previsao();

    this.getPrevisaoByRegiao();
  }

  // Método assíncrono para receber os dados da API
  async getPrevisaoByRegiao() {
    await this.http.get<any>(`${this.URL}` + `${this.TOKEN}`).toPromise().then(
      (resp) => {
        // Resp é a resposta da api
        if (resp) {
          // Salva a resposta da API no objeto de previsão
          this.previsao = resp;

          // Dentro de previsao.data tem um array com os dias previstos
          // Verificar as requisições no Postman para saber como mapear as respostas nos objetos
          this.previsoes = this.previsao.data;
        }
      }
    );
  }
}
